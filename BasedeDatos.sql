create database equipo8;
use equipo8;

create table usuario(
idUsuario int(11)primary key auto_increment not null,
nombre varchar(150) not null,
apellido varchar(150) not null,
edad int(11) not null,
pass varchar(150)not null

);

insert into usuario values(0,'Erick','Garcia',25,'erick123');
insert into usuario values(0,'Roberto','Gomez',25,'roberto123');

create table competencia(
idcompetencia int(11)primary key auto_increment not null,
nombre varchar(20) not null,
descripcion varchar(20)not null,
idUsuario int(11),
constraint foreign key(idUsuario) references usuario(idUsuario)on update cascade on delete cascade
);

insert into competencia values(0,'Persistente','no ',1);
insert into competencia values(0,'Observador','mira',1);
insert into competencia values(0,'Autodidacta','busca',1);

select
c.idcompetencia,
c.nombre,
c.descripcion, 
u.idUsuario,
u.nombre as usuario,
u.apellido 
from competencia c
inner join usuario u on c.idUsuario = u.idUsuario;