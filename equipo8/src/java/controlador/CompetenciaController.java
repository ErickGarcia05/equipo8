/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import conexion.Conexion;
import dao.CompetenciaDao;
import dao.UsuarioDao;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.CompetenciaBean;
import modelo.UsuarioBean;

/**
 *
 * @author roberto.alferesusam
 */
public class CompetenciaController extends HttpServlet {

    Conexion conn = new Conexion();
    CompetenciaDao udao = new CompetenciaDao(conn);
    UsuarioDao usuarioDao = new UsuarioDao(conn);
    boolean res;
    String msg;
    RequestDispatcher rd;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String accion = request.getParameter("accion");
        switch (accion) {

            case "registrar":
                System.out.println("Registrar");
                registrar(request, response);
                break;
            case "consultar":
                consultar(request, response);
                break;
            case "listar":
                listar(request, response);
                break;
        }
    }

    protected void registrar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String nombre = request.getParameter("nombre");
        String descripcion = request.getParameter("descripcion");

        int idU = Integer.parseInt(request.getParameter("idUsuario"));
        UsuarioBean ubean = new UsuarioBean(idU);

        CompetenciaBean cbean = new CompetenciaBean(0);
        cbean.setNombre(nombre);
        cbean.setDescripcion(descripcion);
        cbean.setIdUsuario(ubean);

        res = udao.guardar(cbean);

        if (res) {
            msg = "Inserto Correctamente";
        } else {
            msg = "No Inserto";
        }
        request.setAttribute("msg", msg);
        rd = request.getRequestDispatcher("/Login.jsp");
        rd.forward(request, response);

    }

    protected void listar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //System.out.println("Entro");
        List<CompetenciaBean> lista = udao.consultar();
        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("/listar.jsp");
        rd.forward(request, response);
    }

    protected void consultar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<UsuarioBean> lsUsuario;
        lsUsuario = usuarioDao.consultar();

        request.setAttribute("lista", lsUsuario);
        rd = request.getRequestDispatcher("/RegistrarCompetencia.jsp");
        rd.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

