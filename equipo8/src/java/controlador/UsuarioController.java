/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import conexion.Conexion;
import dao.UsuarioDao;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.UsuarioBean;

@WebServlet(name = "UsuarioController", urlPatterns = {"/usuario"})
public class UsuarioController extends HttpServlet {

    Conexion conn = new Conexion();
    UsuarioDao udao = new UsuarioDao(conn);
    boolean res;
    String msg;
    RequestDispatcher rd;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("accion");
        switch (action) {
            case "registrar":
                registrar(request, response);

            case "listar":
                listar(request, response);
                break;

        }
    }

    protected void registrar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String nombre = request.getParameter("nombre");

        String apellido = request.getParameter("apellido");
        int edad = Integer.parseInt(request.getParameter("edad"));
        String pass = request.getParameter("pass");
        System.out.println(nombre + apellido + edad + pass);
        UsuarioBean ubean = new UsuarioBean(0);
        ubean.setNombre(nombre);
        ubean.setApellido(apellido);
        ubean.setEdad(edad);
        ubean.setPass(pass);
        res = udao.guardar(ubean);
        if (res) {
            msg = "Inserto Correctamente";
        } else {
            msg = "No Inserto";
        }

        request.setAttribute("msg", msg);

        rd = request.getRequestDispatcher("/Login.jsp");
        rd.forward(request, response);

    }

    protected void listar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<UsuarioBean> lista = udao.consultar();
        request.setAttribute("lista", lista);
        rd = request.getRequestDispatcher("/listarUsuario.jsp");
        rd.forward(request, response);

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

}
