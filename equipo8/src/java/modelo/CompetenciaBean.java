/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author roberto.alferesusam
 */
public class CompetenciaBean {

    private int idcompretencia;
    private String nombre;
    private String descripcion;
    private UsuarioBean idUsuario;

    public CompetenciaBean(int idcompretencia) {
        this.idcompretencia = idcompretencia;
    }

    public int getIdcompretencia() {
        return idcompretencia;
    }

    public void setIdcompretencia(int idcompretencia) {
        this.idcompretencia = idcompretencia;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public UsuarioBean getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(UsuarioBean idUsuario) {
        this.idUsuario = idUsuario;
    }

}
