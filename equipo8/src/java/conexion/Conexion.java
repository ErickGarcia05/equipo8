/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author roberto.alferesusam
 */
public class Conexion {

    static String user = "kz";
    static String pass = "kzroot";
    static String base = "equipo8";
    static String url = "jdbc:mysql://usam-sql.sv.cds:3306/" + base + "?useSSL=false";
    Connection conexion;

    public Conexion() {
        try {
            Class.forName("com.mysql.jdbc.Driver");

            conexion = DriverManager.getConnection(url, user, pass);
            if (conexion != null) {
                System.out.println("Conexion exitosa");
            }
        } catch (Exception e) {
            System.out.println("Error al conectar");
        }
    }

    public Connection conectar() {
        return conexion;
    }

    public void desconectar() throws SQLException {
        conexion.close();
    }

//    public static void main(String[] args) {
//        Conexion c = new Conexion();
//        c.conectar();
//    }
}
