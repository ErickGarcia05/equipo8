/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import modelo.UsuarioBean;

/**
 *
 * @author roberto.alferesusam
 */
public class UsuarioDao {

    Conexion conexion;
    PreparedStatement ps;
    ResultSet rs;

    public UsuarioDao(Conexion conexion) {
        this.conexion = conexion;
    }

    public boolean guardar(UsuarioBean usuario) {
        try {
            String sql = "INSERT INTO usuario VALUES(?,?,?,?,?)";
            ps = conexion.conectar().prepareStatement(sql);
            ps.setInt(1, usuario.getIdUsuario());
            ps.setString(2, usuario.getNombre());
            ps.setString(3, usuario.getApellido());
            ps.setInt(4, usuario.getEdad());
            ps.setString(5, usuario.getPass());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean modificar(UsuarioBean usuario) {
        try {
            String sql = "UPDATE usuario SET nombre = ?, apellido = ?, edad = ?, pass = ? WHERE idUsuario = ?";
            ps = conexion.conectar().prepareStatement(sql);
            ps.setString(1, usuario.getNombre());
            ps.setString(2, usuario.getApellido());
            ps.setInt(3, usuario.getEdad());
            ps.setString(4, usuario.getPass());
            ps.setInt(5, usuario.getIdUsuario());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean eliminar(UsuarioBean usuario) {
        try {
            String sql = "DELETE FROM usuario  WHERE idUsuario = ?";
            ps = conexion.conectar().prepareStatement(sql);
            ps.setInt(1, usuario.getIdUsuario());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public List<UsuarioBean> consultar() {
        List<UsuarioBean> lista = new LinkedList<>();
        UsuarioBean usuario;
        try {
            String sql = "SELECT * FROM usuario ";
            ps = conexion.conectar().prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                usuario = new UsuarioBean(rs.getInt("idUsuario"));
                usuario.setNombre(rs.getString("nombre"));
                usuario.setApellido(rs.getString("apellido"));
                usuario.setEdad(rs.getInt("edad"));
                lista.add(usuario);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }

    public List<UsuarioBean> consultarById(UsuarioBean usu) {
        List<UsuarioBean> lista = new LinkedList<>();
        UsuarioBean usuario;
        try {
            String sql = "SELECT * FROM usuario WHERE idUsuario = ?";
            ps = conexion.conectar().prepareStatement(sql);
            ps.setInt(1, usu.getIdUsuario());
            rs = ps.executeQuery();
            while (rs.next()) {
                usuario = new UsuarioBean(rs.getInt("idUsuario"));
                usuario.setNombre(rs.getString("nombre"));
                usuario.setApellido(rs.getString("apellido"));
                usuario.setEdad(rs.getInt("edad"));
                lista.add(usuario);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }

}
