/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import modelo.CompetenciaBean;
import modelo.UsuarioBean;

/**
 *
 * @author roberto.alferesusam
 */
public class CompetenciaDao {

    Conexion conexion;
    PreparedStatement ps;
    ResultSet rs;

    public CompetenciaDao(Conexion conexion) {
        this.conexion = conexion;
    }

    public boolean guardar(CompetenciaBean competencia) {
        try {
            String sql = "INSERT INTO competencia VALUES(?,?,?,?)";
            ps = conexion.conectar().prepareStatement(sql);
            ps.setInt(1, competencia.getIdcompretencia());
            ps.setString(2, competencia.getNombre());
            ps.setString(3, competencia.getDescripcion());
            UsuarioBean usuario = competencia.getIdUsuario();
            ps.setInt(4, usuario.getIdUsuario());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean modificar(CompetenciaBean competencia) {
        try {
            String sql = "UPDATE competencia SET nombre = ?, apellido = ?, edad = ? WHERE idcompetencia = ?";
            ps = conexion.conectar().prepareStatement(sql);
            ps.setString(1, competencia.getNombre());
            ps.setString(2, competencia.getDescripcion());
            UsuarioBean usuario = competencia.getIdUsuario();
            ps.setInt(3, usuario.getIdUsuario());
            ps.setInt(4, competencia.getIdcompretencia());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean eliminar(CompetenciaBean competencia) {
        try {
            String sql = "DELETE FROM competencia  WHERE idcompetencia = ?";
            ps = conexion.conectar().prepareStatement(sql);
            ps.setInt(1, competencia.getIdcompretencia());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public List<CompetenciaBean> consultar() {
        List<CompetenciaBean> lista = new LinkedList<>();
        CompetenciaBean competencia;
        try {
            String sql = "select\n"
                    + "c.idcompetencia,\n"
                    + "c.nombre,\n"
                    + "c.descripcion, \n"
                    + "u.idUsuario,\n"
                    + "u.nombre as usuario,\n"
                    + "u.apellido \n"
                    + "from competencia c\n"
                    + "inner join usuario u on c.idUsuario = u.idUsuario";
            ps = conexion.conectar().prepareStatement(sql);
            rs = ps.executeQuery();
            UsuarioBean usuario;
            while (rs.next()) {

                competencia = new CompetenciaBean(rs.getInt("idcompetencia"));
                competencia.setNombre(rs.getString("nombre"));
                competencia.setDescripcion(rs.getString("descripcion"));
                usuario = new UsuarioBean(rs.getInt("idUsuario"));
                usuario.setNombre(rs.getString("usuario"));
                usuario.setApellido(rs.getString("apellido"));
                competencia.setIdUsuario(usuario);
                lista.add(competencia);
                //System.out.println("entro while");
            }
            return lista;
        } catch (Exception e) {
            System.out.println("Problema");
            return null;
        }
    }

    public List<CompetenciaBean> consultarById(CompetenciaBean com) {
        List<CompetenciaBean> lista = new LinkedList<>();
        CompetenciaBean competencia;
        try {
            String sql = "select c.idcompetencia, c.descripcion, c.nombre, c.idUsuario,"
                    + " u.idUsuario, u.nombre as usuario, u.apellido from competencia as c "
                    + " inner join usuario as u on c.idUsuario = u.idUsuario WHERE idcompetencia = ?;";
            ps = conexion.conectar().prepareStatement(sql);
            ps.setInt(1, com.getIdcompretencia());
            rs = ps.executeQuery();
            UsuarioBean usuario;
            while (rs.next()) {
                usuario = new UsuarioBean(rs.getInt("idUsuario"));
                competencia = new CompetenciaBean(rs.getInt("idcompetencia"));
                competencia.setNombre(rs.getString("nombre"));
                competencia.setDescripcion(rs.getString("descripcion"));
                competencia.setIdUsuario(usuario);
                usuario.setNombre(rs.getString("usuario"));
                usuario.setApellido(rs.getString("apellido"));
                lista.add(competencia);
            }
            return lista;
        } catch (Exception e) {
            return null;
        }
    }
}
