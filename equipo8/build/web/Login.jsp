<%-- 
    Document   : Login
    Created on : 01-24-2020, 08:39:18 AM
    Author     : erick.garciausam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="resourse/css/css.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="wrapper fadeInDown">
            <div id="formContent">
                <!-- Tabs Titles -->
                <h2 class="active">Inicio de Sesion </h2>
                <h2 class="inactive underlineHover"><a href="registroUsuario.jsp">Registrar</a></h2>

                <!-- Icon -->
                <div class="fadeIn first">
                    <img src="resourse/img/icono.png" id="icon" alt="User Icon" />
                </div>

                <!-- Login Form -->
                <form>
                    <input type="text" id="login" class="fadeIn second" name="login" placeholder="login">
                    <input type="password" id="password" class="fadeIn third" name="login" placeholder="password">
                    <input type="submit" class="fadeIn fourth" value="Log In">
                </form>

                <!-- Remind Passowrd -->
                <div id="formFooter">
                    <a class="underlineHover" href="#">Forgot Password?</a>
                </div>

            </div>
        </div>
    </body>
</html>
