<%-- 
    Document   : RegistrarCompetencia
    Created on : 01-24-2020, 10:35:53 AM
    Author     : roberto.alferesusam
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="resourse/css/css.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="wrapper fadeInDown">
            <div id="formContent">
                <!-- Tabs Titles -->
                <h2 class="active"> RegistrarCompetencia</h2>
                <h2 class="inactive underlineHover"><a href="menu.jsp">Listas</a></h2>

                <!-- Icon -->
                <!-- Login Form -->
                <form action="Competencia?accion=registrar" method="post" autocomplete="false">
                    <input type="text" id="login" class="fadeIn second" name="nombre" placeholder="Nombre">
                    <input type="text" id="login" class="fadeIn third" name="descripcion" placeholder="Descripcion">
                    <select  name="idUsuario" id="login" class="fadeIn third">
                        <c:forEach items="${lista}" var="lsUs">
                            <option value="${lsUs.idUsuario}">${lsUs.nombre} ${lsUs.apellido}</option>
                        </c:forEach>    
                    </select>

                    <input type="submit" class="Guardar" value="Guardar">
                </form>
            </div>
    </body>
</html>
